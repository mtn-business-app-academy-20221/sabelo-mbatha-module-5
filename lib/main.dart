import "package:flutter/material.dart";
import "package:sabelo_mbatha_module_5/m5File1.dart";
import "package:sabelo_mbatha_module_5/m5File2.dart";
import "package:animated_splash_screen/animated_splash_screen.dart";
import "package:page_transition/page_transition.dart";
import "package:lottie/lottie.dart";
import "package:firebase_core/firebase_core.dart";

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
        apiKey: "AIzaSyBZ3tGZAXpsWmGvI5777an-K2GOrjsvqWY",
        authDomain: "sabelo-mbatha-module-5.firebaseapp.com",
        appId: "1:845095722893:web:8f3f0ab69cd4ac7a2967c4",
        messagingSenderId: "845095722893",
        projectId: "sabelo-mbatha-module-5",
    storageBucket: "sabelo-mbatha-module-5.appspot.com",)
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const MySplashScreen(),
      routes: {
        "/login": (context) => const Login(),
        "/userProfile": (context) => const UserProfile(),
        "/Registration": (context) => const Registration(),
        "/Dashboard": (context) => const Dashboard(),
        "/appsPage1": (context) => const AppWinnersPage1(),
        "/appsPage2": (context) => const AppWinnersPage2(),
        "/appsPage3": (context) => const AppWinnersPage3(),
      },
    );
  }
}

class MySplashScreen extends StatelessWidget {
  const MySplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.black, Colors.grey.shade700],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            stops: const [0.35, 0.9],
          )),
      child: AnimatedSplashScreen(
        splash: Lottie.asset("asset/Yello.json"),
        backgroundColor: Colors.transparent,
        nextScreen: const Login(),
        duration: 5000,
        splashTransition: SplashTransition.fadeTransition,
        splashIconSize: 250,
        pageTransitionType: PageTransitionType.leftToRightWithFade,
        animationDuration: const Duration(
          seconds: 6,
        ),
      ),
    );
  }
}
