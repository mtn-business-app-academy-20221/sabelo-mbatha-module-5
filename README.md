# Sabelo Mbatha Module 5

An exercise to create a project on Firebase and connect your web app with forms to your app with at 
least 3 input fields as well as to connect the app to the database to create, read, update and delete.

## Instructions 
- Set up a Firebase account.
- Create a Gitlab account.
- Create a repository on Gitlab as name-surname-module-5

## Further Instructions
- Create a project on Firebase
- Connect your web app to Firebase
- Add a form to your app with at least 3 input fields 
- Write code to connect your app to the database to create, read, update and delete

### END!